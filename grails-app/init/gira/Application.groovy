package gira

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import io.codearte.jfairy.Fairy
import org.springframework.boot.Banner
import org.springframework.context.annotation.Bean

class Application extends GrailsAutoConfiguration {

    static void main(String[] args) {
        final GrailsApp app = new GrailsApp(Application)
        Banner giraBanner = new GiraBanner()
        app.setBanner(giraBanner)
        app.setBannerMode(Banner.Mode.CONSOLE)
        app.run(args)
    }

    @Bean
    Fairy initFairy() {
        return Fairy.create(Locale.forLanguageTag("fr"))
    }
}