package gira

import grails.util.Environment
import io.codearte.jfairy.Fairy
import io.codearte.jfairy.producer.person.Person
import org.springframework.beans.factory.annotation.Autowired

class BootStrap {

    @Autowired
    Fairy fairy

    def init = { servletContext ->
        if (Environment.current in [Environment.DEVELOPMENT, Environment.TEST]) {
            this.loadData()
        }
    }

    def destroy = {
    }

    def loadData() {
        log.info "Loading data into env: $Environment.current"
        100.times {
            Person p = fairy.person()
            new User(login: p.username, fullname: p.fullName, email: p.email).save()
        }
    }
}
