package gira

import grails.gorm.transactions.Transactional

@Transactional
class UserService {

    User get(int id) {
        return User.get(id)
    }
}
