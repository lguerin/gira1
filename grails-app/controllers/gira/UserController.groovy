package gira


import grails.rest.*

class UserController extends RestfulController {

    static responseFormats = ['json', 'xml']

    def userService

    UserController() {
        super(User)
    }

    def hello() {
        User user = userService.get(params.id)
        render "Hello $user.fullname"
    }
}
