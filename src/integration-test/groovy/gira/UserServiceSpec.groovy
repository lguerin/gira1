package gira


import grails.testing.mixin.integration.Integration
import grails.transaction.*
import spock.lang.*

@Integration
@Rollback
class UserServiceSpec extends Specification {

    UserService userService

    def setup() {
        Fixtures.buildUser()
    }

    def cleanup() {
    }

    void "Should get one user"() {
        when:
        User user = userService.get(1)

        then:
        user != null
        user.login != null
        user.email.contains('@')
    }
}
