package gira

import grails.testing.gorm.DomainUnitTest
import grails.testing.web.controllers.ControllerUnitTest
import org.grails.testing.GrailsUnitTest
import spock.lang.Specification

class UserControllerUnitSpec extends Specification implements ControllerUnitTest<UserController>, DomainUnitTest<User> {

    def setup() {
        def userService = Mock(UserService)
        userService.get(_ as Integer) >> Fixtures.buildUser([fullname: 'Guillaume Laforge'])
        controller.userService = userService
    }

    def cleanup() {
    }

    void "Should say hello to current user"() {
        when:
        controller.params.id = 1
        controller.hello()

        then:
        status == 200
        response.text == 'Hello Guillaume Laforge'
    }
}