package gira

class Fixtures {

    /**
     * User builder for tests
     *
     * @param params
     * @param toSave
     * @return
     */
    static User buildUser(params = [:], toSave = true) {
        def login = "login_${UUID.randomUUID()}"
        params = [
            login: params.login ?: login,
            fullname: params.fullname ?: 'Guillaume Laforge',
            email: params.email ?: "${login}@ifap.nc"
        ]
        return toSave ? new User(params).save() : new User(params)
    }
}
