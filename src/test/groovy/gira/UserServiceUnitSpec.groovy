package gira

import grails.testing.gorm.DomainUnitTest
import grails.testing.services.ServiceUnitTest
import spock.lang.Specification

class UserServiceUnitSpec extends Specification implements ServiceUnitTest<UserService>, DomainUnitTest<User> {

    def setup() {
        Fixtures.buildUser()
    }

    def cleanup() {
    }

    void "Should return User from the service"() {
        when:
        User user = service.get(1)

        then:
        user != null
    }
}
